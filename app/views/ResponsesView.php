<?php

/**
 * views/Responsess.php
 *
 * ResponsesView View
 * with overrides to the Base View
 *
 * @author     Paul Riley
 * */

namespace Contact\Views;

use Contact\Models\Response;

class ResponsesView extends BaseView
{


    public function render($token, $message = null, Response $response = null)
    {
        echo $this->header();

        if (isset($response)) {
            echo "<div class='results'>";
            echo "<div><strong>$message</strong></div>";
            echo "<div><strong>Name:</strong>" . $this->filter($response->name) . "</div>";
            echo "<div><strong>Email:</strong>" . $this->filter($response->email) . "</div>";
            echo "<div><strong>Phone:</strong>" . $this->filter($response->phone) . "</div>";
            echo "<div><strong>Message:</strong>" . $this->filter($response->message) . "</div>";
            echo "<div><strong>Optin:</strong>" . $this->filter($response->message) . "</div>";
            echo "</div>";
        } else {
            echo "<div class='errors'>";
            echo "<div><strong>$message</strong></div>";
            echo "</div>";
        }
        echo $this->form($token);
        echo $this->footer();
    }

    public function form($token)
    {
        $emailValidation = getenv('EMAILVALIDATION');
        $email           = isset($emailValidation) ? "pattern = '$emailValidation'" : "";
        $phoneValidation = getenv('PHONEVALIDATION');
        $phone           = isset($emailValidation) ? "pattern = '$phoneValidation'" : "";

        //could use htm5 email/ tel types but use patterns for validation so we can use the same backend
        $result = "<form class='contact_form' action='$this->baseurl/' method='POST'>
				<input type='hidden' name='token' value='$token' ?>

				<label> Name*:</label>
				<input id='name' name='name' required/>
				
				<label>Phone*:</label>
				<input type='text' 
					$phone
				name='phone' 
				id='phone' 
				placeholder='Enter a valid UK phone number' required />
				
				<label>Email*:</label>
				<input id='email'   
						name='email'
						type='text'
						$email
						placeholder='Enter a email' required/>
				
				<label>Message:</label>
				<div class='text_area'>
					<textarea name='message' id='message' rows='4' cols='50' minlength='25' maxlength='255'></textarea>
				</div>
				
				<label>Opt in to newsletter:</label>
				<input type='checkbox' id='optin' name='optin' value='true'>
				
				<input class='button' type='submit' value='Submit'>
				<form>";

        return $result;
    }

}
