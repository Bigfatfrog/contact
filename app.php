<?php

/**
 * app.php
 *
 * @author     Paul Riley
 * */

$loader = require __DIR__ . '/vendor/autoload.php';

use Contact\Controllers\Responses;
use Contact\Services\Env;

class app
{

    public function __construct()
    {
        Env::set();
    }


    public function router()
    {
        $controller = new Responses();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // submit the form
            $controller->responses_post();
        } else {
            $controller->responses_get();
        }
    }

}
