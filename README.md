#Contact Test

The application uses a MVC style architecture. 
A .env file used initalise the setup parameters. 
The .env file also contains regex patterns for phone and email vaildation. This is to ensure frontend and backend validation is exactly the same.

##Installation
- The project uses Composer (getcomposer.org) to initiate the autoload of the classes. It has a dev dependeny of PHP Unit that was sued for some testing.
- A .env file is required with the URL and DB connection paratmeters - an example (.env.example) is provides
- The web server entry is /public
- contact.sql provides the mysql database definitions
