<?php

/**
 * controllers/ResponsesViewView.php
 *
 * ResponsesView Controller
 * with overrides to the Base Controller
 *
 * @author     Paul Riley
 * */

namespace Contact\Controllers;

use Contact\Models\Response as ResponseModel;
use Contact\Views\ResponsesView;

class Responses extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->view = new ResponsesView();
    }

    public function responses_get()
    {
        $token = $this->setToken();
        // display the form
        $this->view->render($token);
    }

    public function responses_post()
    {
        $response = new ResponseModel();

        $response->name    = isset($_POST['name']) ? $_POST['name'] : null;
        $response->phone   = isset($_POST['phone']) ? $_POST['phone'] : null;
        $response->email   = isset($_POST['email']) ? $_POST['email'] : null;
        $response->message = isset($_POST['message']) ? $_POST['message'] : null;
        $response->optin   = isset($_POST['optin']) ? $_POST['optin'] : null;
        $response->ip      = $this->getIP();

        $result = false;
        if ($this->checkToken()) {
            if (isset($response->name) && isset($response->phone) && isset($response->email)) {
                $result = $response->insert();
            }
            $message = $result ? 'Saved!' : 'Save error.';

            // email admin
            if ($result && $response->optin === 1) {
                $message = "Name:$response->name Phone:$response->phone Email:$response->email Message:$response->message";
                mail(getenv('EMAIL'), 'Subscription', $message);
            }
        } else {
            $response = null;
            $message  = "There has been an error!";
        }

        $token = $this->setToken();
        $this->view->render($token, $message, $response);
    }
}
