<?php

/**
 * models/BaseAbstract.php
 *
 * Base Abstract
 * functionality shared between all models
 *
 * @author     Paul Riley
 * */

namespace Contact\Models;

use Contact\Services\Database;

abstract class BaseAbstract
{

    public function insert()
    {
        $data = [];
        foreach ($this->fillable as $field) {
            $data[$field] = isset($this->$field) ? $this->$field : null;
        }

        return Database::insert($this->table, $data);
    }

}
