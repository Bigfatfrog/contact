<?php

/**
 * database.php
 *
 * @author     Paul Riley
 * */

namespace Contact\Services;

class Database
{

    private static $conn;

    public static function insert($table, $values = array())
    {
        try {
            $connection = self::connection();

            $columns = array_keys($values);

            $sql = 'INSERT INTO ' . $table;
            $sql .= '(' . implode(',', $columns) . ') ';
            $sql .= 'VALUES (' . implode(',', array_fill(0, count($values), '?')) . ')';

            $result = $connection->prepare($sql);
            $result->execute(array_values($values));

            return $result;
        } catch (\PDOException $e) {
            die("Insert failed: " . $e->getMessage());
        }
    }

    public static function connection()
    {
        if (is_null(self::$conn)) {
            try {
                $hostName = getenv('SERVER');
                $dbName   = getenv('DATABASE');
                $userName = getenv('USER');
                $password = getenv('PASSWORD');

                self::$conn = new \PDO("mysql:host=$hostName;dbname=$dbName;", $userName, $password);
                // set the PDO error mode to exception
                self::$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                //echo "Connected successfully";
            } catch (\PDOException $e) {
                die("Connection failed: " . $e->getMessage());
            }
        }

        return self::$conn;
    }

}
