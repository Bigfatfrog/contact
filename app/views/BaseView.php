<?php

/**
 * views/BaseView.php
 *
 * BaseView
 * with shared view methods
 *
 * @author     Paul Riley
 * */

namespace Contact\Views;


abstract class BaseView
{

    public function __construct()
    {
        $this->baseurl = getenv('BASEURL');
    }

    public function filter($value)
    {
        return htmlentities($value, ENT_QUOTES, 'UTF-8');
    }

    protected function header()
    {
        $result = "<!DOCTYPE html>
            <html>
                <head>
                    <title>Contact</title>"
                  . $this->include_bootstrap() .
                  "</head>
                <body>
                <div class='container'>
                    <h1>Subscribe</h1>
                </div>";

        return $result;
    }

    protected function include_bootstrap()
    {
        $css = "<link rel='stylesheet' href='$this->baseurl/resources/style.css' >";

        return $css;
    }

    protected function footer()
    {
        $result = "    </body>
                   </html>";

        return $result;
    }

}
