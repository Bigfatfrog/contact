<?php

/**
 * env.php
 *
 * @author     Paul Riley
 * */

namespace Contact\Services;

class Env
{

    public static function set()
    {
        $filename = file_exists(".env") ? ".env" : "../.env";
        if (file_exists($filename)) {
            $handle = fopen($filename, "r");
            if ($handle) {
                while (($line = fgets($handle)) !== false) {
                    // process the line read.
                    putenv(rtrim($line));
                }
                fclose($handle);
            }
        } else {
            die("no .env file");
        }
    }

}
