<?php

/**
 * controllers/BaseController.php
 *
 * BaseController
 * with shared controller methods
 *
 * @author     Paul Riley
 * */

namespace Contact\Controllers;


abstract class BaseController
{


    public function __construct()
    {
        session_start();
    }

    public function setToken()
    {
        // set csrf token in a php session
        $token             = md5(uniqid());
        $_SESSION['token'] = $token;

        return $token;
    }

    public function checkToken()
    {
        //check the csfr request token is the same as returned
        $token = isset($_SESSION['token']) ? $_SESSION['token'] : "";

        return $_POST['token'] === $token;
    }

    public function sanitizeInput()
    {
        foreach ($_POST as $key => $value) {
            switch ($key) {
                case('email'):
                    $_POST[$key] = filter_var($value, FILTER_SANITIZE_EMAIL);
                    break;
                default:
                    $_POST[$key] = filter_var($value, FILTER_SANITIZE_STRING);
                    break;
            }
        }
    }

    public function getIp()
    {
        if ( ! empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ( ! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }
}
