<?php

/**
 * models/Response.php
 *
 * ResponsesView Model
 * with overrides to the Base Abstract
 *
 * @author     Paul Riley
 * */

namespace Contact\Models;

class Response extends BaseAbstract
{
    protected $table = "response";
    protected $fillable = ['name', 'ip', 'email', 'message', 'optin'];


    public function __set($name, $value)
    {
        $valid = false;
        switch ($name) {
            case('email'):
                $emailValidation = '/' . getenv('EMAILVALIDATION') . '/';
                if (isset($emailValidation) && ! preg_match($emailValidation, $value)) {
                    $valid = false;
                } else {
                    $valid = true;
                }
                break;
            case('phone'):
                $phoneValidation = '/' . getenv('PHONEVALIDATION') . '/';
                if (isset($phoneValidation) && ! preg_match($phoneValidation, $value)) {
                    $valid = false;
                } else {
                    $valid = true;
                }
                break;
            case('optin'):
                $valid = true;
                $value = $value === 'true' ? 1 : null;
                break;
            case('message'):
                $valid = true;
                $value = strlen($value) >= 25 && strlen($value) <= 255 ? $value : null;
                break;
            default:
                $valid = true;
                break;
        }
        if ($valid === true) {
            $this->$name = $value;
        }
    }
}
